This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## El Observador Web

Siga los siguientes pasos:


1. Clonar proyecto e ingresar al directorio
2. Ejecutar ```npm install```
3. Levantar el servicio: ```npm run dev```
4. El frontend se levanta en el puerto 3001 y la api en el puerto 3000
```
http://localhost:3001
```
5. Utilizar los siguientes usuarios
```
Suscriptor:
Username: suscriptor
Password: suscriptor

Redactor 1:
Username: redactor
Password: redactor

Redactor 2:
Username: redactor2
Password: redactor2

Editor:
Username: editor
Password: editor

```
6. Puede crar mas suscriptores en http://localhost:3001/register
