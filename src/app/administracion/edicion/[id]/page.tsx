"use client";

import { useSession } from "next-auth/react";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import styles from "../../../../styles/Principal.module.css";
import { toast } from "react-toastify";

const ActualizarNoticiaPage = ({params} :{params:{id:number}}) => {
  const { data: session, status } = useSession();
  const [errors, setErrors] = useState<string>("");
  const router = useRouter();
  
  const [noticia, setNoticia] = useState<any>();

  const [titulo, setTitulo] = useState<string>("");
  const [lugar, setLugar] = useState<string>("");
  const [contenido, setContenido] = useState<string>("");

  const getNoticia = async () => {
    const res = await fetch(`${process.env.NEXT_PUBLIC_BACKEND_URL}/noticias/${params.id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        authorization: `Bearer ${session?.user?.token}`,        
      },
    });
    const data = await res.json();

    setTitulo(data.result.titulo);
    setLugar(data.result.lugar);
    setContenido(data.result.contenido);
    setNoticia(data.result);
  };


  useEffect(() => { setTimeout(() => {
    getNoticia();
  }, 1000); }, [session]);

  const actualizarNoticia = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setErrors("");
    const res = await fetch(`${process.env.NEXT_PUBLIC_BACKEND_URL}/noticias/${params.id}`, {
        method: "PATCH",
        headers: {
          "Content-Type": "application/json",
          authorization: `Bearer ${session?.user?.token}`,
        },
        body: JSON.stringify({
          ... noticia,
          titulo,
          lugar,
          contenido,
        }),
      }
    );

    const responseAPI = await res.json();
    if (!responseAPI.status) {
      setErrors(responseAPI.message);
      return;
    }
    else{
       toast.success('Registro Actualizado');
       router.push("/administracion");
    } 
  };

  if (status === "loading") return <p>Loading...</p>
  if (!noticia) return null;
  return (
    <div className="page-container">
      <div className={styles.main}>
        <h1>Editar Noticia</h1>
        <form onSubmit={actualizarNoticia}>
          <input
            type="text"
            placeholder="Titulo de la noticia"
            name="name"
            className="form-control mb-2"
            value={titulo}
            onChange={(event) => setTitulo(event.target.value)}
          />
          <input
            type="text"
            placeholder="Lugar"
            name="lugar"
            className="form-control mb-2"
            value={lugar}
            onChange={(event) => setLugar(event.target.value)}
          />
          <textarea 
            rows={5}
            placeholder="Contenido de la noticia"
            name="contenido"
            className="form-control mb-2"
            value={contenido}
            onChange={(event) => setContenido(event.target.value)}
          />
          <button type="submit" className="btn btn-primary">
            Actualizar
          </button>
        </form>

        {errors && (
          <div className="alert alert-danger mt-2">
            <ul className="mb-0">
              <li key={errors}>{errors}</li>
            </ul>
          </div>
        )}
      </div>
    </div>
  );
};
export default ActualizarNoticiaPage;
