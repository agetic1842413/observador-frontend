"use client";

import { useSession } from "next-auth/react";
import { useRouter } from "next/navigation";
import { useState } from "react";
import styles from "../../../styles/Principal.module.css";

const RegistroNoticiaPage = () => {
  const { data: session, status } = useSession();
  const [errors, setErrors] = useState<string>("");
  const router = useRouter();
  
  const [titulo, setTitulo] = useState<string>("");
  const [lugar, setLugar] = useState<string>("");
  const [contenido, setContenido] = useState<string>("");

  const registrarNoticia = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setErrors("");

    const res = await fetch(
      `${process.env.NEXT_PUBLIC_BACKEND_URL}/noticias`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          authorization: `Bearer ${session?.user?.token}`,
        },
        body: JSON.stringify({
          titulo,
          lugar,
          contenido,
        }),
      }
    );

    const responseAPI = await res.json();
    console.log(responseAPI);
    if (!responseAPI.status) {
      setErrors(responseAPI.message);
      return;
    }
    else{
       router.push("/administracion");
    } 
  };

  if (status === "loading") {
    return <p>Loading...</p>;
  }
  
  return (
    <div className="page-container">
      <div className={styles.main}>
        <h1>Nueva Noticia</h1>
        <form onSubmit={registrarNoticia}>
          <input
            type="text"
            placeholder="Titulo de la noticia"
            name="name"
            className="form-control mb-2"
            value={titulo}
            onChange={(event) => setTitulo(event.target.value)}
          />
          <input
            type="text"
            placeholder="Lugar"
            name="lugar"
            className="form-control mb-2"
            value={lugar}
            onChange={(event) => setLugar(event.target.value)}
          />
          <textarea 
            rows={5}
            placeholder="Contenido de la noticia"
            name="contenido"
            className="form-control mb-2"
            value={contenido}
            onChange={(event) => setContenido(event.target.value)}
          />
          <button type="submit" className="btn btn-primary">
            Registrar
          </button>
        </form>

        {errors && (
          <div className="alert alert-danger mt-2">
            <ul className="mb-0">
              <li key={errors}>{errors}</li>
            </ul>
          </div>
        )}
      </div>
    </div>
  );
};
export default RegistroNoticiaPage;
