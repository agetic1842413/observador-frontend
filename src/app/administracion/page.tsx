"use client";
import { NoticiaModel } from "@/types";
import { useSession } from "next-auth/react";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import styles from "../../styles/Principal.module.css";
import { toast } from "react-toastify";

const noticias = () => {
  const router = useRouter();
  const [noticias, setNoticias] = useState<any[]>([]);
  const { data: session, status } = useSession();

  const getNoticias = async () => {
      const res = await fetch(`${process.env.NEXT_PUBLIC_BACKEND_URL}/noticias`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          authorization: `Bearer ${session?.user?.token}`,
        },
      });
      const data = await res.json();
      setNoticias(data.result);
  };

  useEffect(() => { setTimeout(() => {
    getNoticias();
  }, 1000); }, [session]);



  const eliminarNoticia = async (id:number) => {
    const res = await fetch(`${process.env.NEXT_PUBLIC_BACKEND_URL}/noticias/${id}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        authorization: `Bearer ${session?.user?.token}`,
      },
    });
    const content = await res.json();
    if(content.status)
    {
      setNoticias(noticias?.filter((post:NoticiaModel)=>{  return post.id !== id  }));
      toast.success("Noticia Eliminada");
    }else{
      toast.error(content.message);
    }
  }

  let publicarNoticia = async (id:number) => {
    const res = await fetch(`${process.env.NEXT_PUBLIC_BACKEND_URL}/noticias/publicar/${id}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        authorization: `Bearer ${session?.user?.token}`,
      },
    });
    const content = await res.json();
    if(content.status)
    {
      toast.success("Noticia Públicada");
      getNoticias();
    }
  }

  
  if (status === "loading") {
    return <p>Loading...</p>;
  }

  return (
    <div className="w-full">
      <table className="w-full">
        <tbody>
          <tr><td colSpan={4}>
              <button onClick={() => router.push(`/administracion/registro`)}
                    className="bt bt-primary">
                    Nuevo
                  </button>
            </td></tr>
        </tbody>
      </table>
      <br />
      <table className={styles.widthFull}>
        <thead>
          <tr className="text-center">
            <th className="border border-slate-300">Id</th>
            <th className="border border-slate-300">Titulo</th>
            <th className="border border-slate-300">Autor</th>
            <th className="border border-slate-300">Contenido</th>
            <th className="border border-slate-300">Estado</th>
            <th className="border border-slate-300">Fecha Publicación</th>
            <th className="border border-slate-300">Editar</th>
          </tr>
        </thead>
        <tbody>
          {noticias &&
            noticias.map((item: any) => (
              <tr key={item.id}>
                <td className="w-10 border border-slate-300 text-center">
                  {item.id}
                </td>
                <td className="border border-slate-300">{item.titulo}</td>
                <td className="border border-slate-300 text-center">
                  {item.autor}
                </td>
                <td className="border border-slate-300 text-center">
                  {item.contenido}
                </td>
                <td className="border border-slate-300 text-center">
                  {item.estado == "e" ? "En Edición" : "Publicado"}
                </td>
                <td className="border border-slate-300 text-center">
                  {item.fechaPublicacion}
                </td>
                <td className="w-52 border border-slate-300">
                {session?.user.roles.includes('editor') && <button disabled={item.estado=='p'}
                    onClick={() => publicarNoticia(item.id)}
                    className="bt bt-secondary"
                  >
                    Publicar
                  </button>}
                  
                  <button
                    onClick={() => router.push(`/administracion/edicion/${item.id}`)}
                    className="bt bt-secondary"
                  >
                    Editar
                  </button>
                  <button
                    onClick={() => eliminarNoticia(item.id)}
                    className="bt bt-danger"
                  >
                    Eliminar
                  </button>
                </td>
              </tr>
            ))}
        </tbody>
      </table>
    </div>
  );
};
export default noticias;
