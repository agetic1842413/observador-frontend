"use client";
import { useEffect, useState } from 'react';
import styles from '../styles/Principal.module.css'
import { useSession } from 'next-auth/react';
import { toast } from 'react-toastify';


const HomePage = () => {
  const [noticias, setNoticias] = useState<any[]>([])
  const { data: session } = useSession();

  const getNoticias = async () => {
    const res = await fetch(`${process.env.NEXT_PUBLIC_BACKEND_URL}/noticias-publicadas?cantidad=5`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"        
      },
    });
    const data = await res.json();
    setNoticias(data.result);
  };

  const notify = () => toast("Funcionalidad no implementada");

  useEffect(() => {
    setTimeout(() => {
      getNoticias();
    }, 500);
  }, []);

  return (
    <div className="page-container">
      
      <div className={styles.main}>
        <h1>EL OBSERVADOR</h1>
        <h3>Tu centro de noticias</h3>
        <h3> Ultimas noticias del sitio</h3>
        {/* <pre>
        <code>{JSON.stringify(noticias)}</code>
        
        </pre> */}
        <div>
            <ul>
                {noticias.map( toDo =>
                    <li key={toDo.id}>
                      <span> <h4><b> {toDo.titulo}</b></h4> </span>
                      <b>Autor:</b> { toDo.autor}, <b>Fecha de Publicación:</b> { toDo.fechaPublicacion} <br />
                      {/* <span>( {toDo.estado=='p' ? 'PUBLICADO' : 'EN EDICION'} )</span><br /> */}
                      <span>{toDo.contenido}</span><br />
                      {session && session?.user?.roles.includes('suscriptor') && <button
                      className="bt bt-secondary"
                      onClick={notify}
                      >
                        Comentar
                      </button>}

                      <br />
                    </li>
                // <li>
                //     <span>{JSON.stringify(toDo.titulo)}</span>
                // </li>
                )}
            </ul>
        </div>
      </div>
    </div>
  );
};
export default HomePage;
