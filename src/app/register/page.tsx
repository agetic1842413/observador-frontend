"use client";

import { signIn } from "next-auth/react";
import { useRouter } from "next/navigation";
import { useState } from "react";
import styles from "../../styles/Principal.module.css";

const RegisterPage = () => {
  const [errors, setErrors] = useState<string>("");
  const [nombreCompleto, setNombreCompleto] = useState<string>("");
  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const router = useRouter();

  const registrarUsuario = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setErrors("");

    const res = await fetch(
      `${process.env.NEXT_PUBLIC_BACKEND_URL}/auth/register`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          nombreCompleto,
          username,
          password,
        }),
      }
    );

    const responseAPI = await res.json();
    console.log(responseAPI);
    if (!responseAPI.status) {
      setErrors(responseAPI.message);
      return;
    } else {
      const responseNextAuth = await signIn("credentials", {
        username,
        password,
        redirect: false,
      });

      if (responseNextAuth?.error) {
        setErrors(responseNextAuth.error.toString());
        return;
      }

      router.push("/");
    }
  };

  return (
    <div className="page-container">
      <div className={styles.main}>
        <h1>Nuevo Suscriptor</h1>
        <form onSubmit={registrarUsuario}>
          <input
            type="text"
            placeholder="Nombre Completo"
            name="name"
            className="form-control mb-2"
            value={nombreCompleto}
            onChange={(event) => setNombreCompleto(event.target.value)}
          />
          <input
            type="text"
            placeholder="username"
            name="username"
            className="form-control mb-2"
            value={username}
            onChange={(event) => setUsername(event.target.value)}
          />
          <input
            type="password"
            placeholder="password"
            name="password"
            className="form-control mb-2"
            value={password}
            onChange={(event) => setPassword(event.target.value)}
          />
          <button type="submit" className="btn btn-primary">
            Registrar
          </button>
        </form>

        {errors && (
          <div className="alert alert-danger mt-2">
            <ul className="mb-0">
            <li key={errors}>{errors}</li>
            </ul>
          </div>
        )}
      </div>
    </div>
  );
};
export default RegisterPage;
