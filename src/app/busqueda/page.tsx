"use client";
import { useEffect, useState } from 'react';
import styles from '../../styles/Principal.module.css'
import styles2 from '../../styles/NavBar.module.css'
import { toast } from 'react-toastify';


const HomePage = () => {
  const [noticias, setNoticias] = useState<any[]>([])
  const [termino, setTermino] = useState<string>("");

  const buscarNoticias = async () => {
    const res = await fetch(`${process.env.NEXT_PUBLIC_BACKEND_URL}/noticias-publicadas?termino=${termino}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"        
      },
    });

    const data = await res.json();
    setNoticias(data.result);
    toast.info(data.result.length+ " Noticia(s) encontrada(s)");
  };

  return (
    <div className="page-container">
      <div className={styles.main}>
        <h1>Buscar Noticia</h1>
        
        <div className={styles2.main}>
          <input
            type="text"
            placeholder="Palabra clave"
            name="name"
            className="form-control mb-2"
            value={termino}
            onChange={(event) => setTermino(event.target.value)}
          />
          <button className="btn btn-primary" onClick={() => buscarNoticias()}>
            Buscar
          </button>
        </div>

        <div>
            <ul>
                {noticias.map( toDo =>
                    <li key={toDo.id}>
                      <span> <h4><b> {toDo.titulo}</b></h4> </span>
                      <b>Autor:</b> { toDo.autor}, <b>Fecha de Publicación:</b> { toDo.fechaPublicacion} <br />
                      {/* <span>( {toDo.estado=='p' ? 'PUBLICADO' : 'EN EDICION'} )</span><br /> */}
                      <span>{toDo.contenido}</span><br />
                      <br />
                    </li>
                )}
            </ul>
        </div>
      </div>
    </div>
  );
};
export default HomePage;
