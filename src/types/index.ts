export  interface NoticiaModel{
    id:number,
    titulo:string,
    autor:string,
    contenido:string,
}
