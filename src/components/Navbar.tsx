"use client";

import { signOut, useSession } from "next-auth/react";
import { useRouter } from "next/navigation";
import styles from "../styles/NavBar.module.css"

const Navbar = () => {
  const router = useRouter();
  const { data: session } = useSession();

  return (
    <div className={styles.main}>
            {session?.user && <div>Usuario: {session?.user.username}</div>}
            <div onClick={() => router.push("/")}>Inicio</div>
            <div onClick={() => router.push("/busqueda")}>Busqueda</div>
            {session?.user ? (<>
              {!session?.user?.roles.includes('suscriptor') && <div onClick={() => router.push("/administracion")}>Administración</div>}
              <div onClick={() => { signOut({ redirect: false }).then(() => { router.push("/"); }); }}>Salir</div>
            </>
            ) : (
              <>
            <div onClick={() => router.push("/login")}>Entrar</div>
            <div onClick={() => router.push("/register")}>Registrarse</div>
            </>
        )}
    </div>
    
  );
};
export default Navbar;
