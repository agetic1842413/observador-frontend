  declare module "next-interfaces" {
    interface Noticia {
        titulo: string;
        lugar: string;
        autor: string;
        contenido: string;
        estado: string;
      }
    
  }
  